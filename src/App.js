import logo from './logo.svg';
import { LinkToGoogleForm } from './features/LinkToGoogleForm';
import './App.css';

const googleFormHash = 'JzDv5DRWSjCcy7EU8';

function App() {
  return (
    <div className="App">
      <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <LinkToGoogleForm hash={googleFormHash}></LinkToGoogleForm>
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
      </header>
    </div>
  );
}

export default App;